# About
**CS 583 3D Game**

Welcome to Orb Runner.  You control an orb as it traverses through moving platforms and obstacles galore. 
Your goal is to reach the end of the level, where you'll find a green ring to jump through to win!

Good luck!  

**Authors**
* Gerardo Lopez
* Mark Rassamni
* Omar Wardak
* William Merz

# Wiki Links
* [Instructions](https://gitlab.com/OmarW/Orb_Runner/wikis/Instructions)
* [Asset sources](https://gitlab.com/OmarW/Orb_Runner/wikis/Assets)
* [Requirements fulfilled](https://gitlab.com/OmarW/Orb_Runner/wikis/Requirements)
